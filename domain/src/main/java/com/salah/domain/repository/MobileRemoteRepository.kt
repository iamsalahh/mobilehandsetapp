package com.salah.domain.repository

import com.salah.model.MobileHandset
import io.reactivex.Single
interface MobileRemoteRepository {

    fun getMobileHandsets(): Single<List<MobileHandset>>
}