package com.salah.domain.interactor

import com.salah.domain.repository.MobileRemoteRepository
import com.salah.model.MobileHandset
import io.reactivex.Single

class GetMobileUseCase(private val mobileRemoteRepository: MobileRemoteRepository) {

    fun execute(): Single<List<MobileHandset>> {
        return mobileRemoteRepository.getMobileHandsets()
    }

}