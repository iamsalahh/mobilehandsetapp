package com.salah.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.salah.R
import com.salah.databinding.FilterBottomSheetBinding
import com.salah.ui.home.viewmodel.MobileViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import com.warkiz.widget.IndicatorSeekBar

import com.warkiz.widget.SeekParams

import com.warkiz.widget.OnSeekChangeListener




class FilterBottomSheet : BottomSheetDialogFragment() {
    private var _binding: FilterBottomSheetBinding? = null
    private val binding get() = _binding!!
    private lateinit var listener : OnActionCompleteListener
    private val mobileViewModel: MobileViewModel by sharedViewModel()

    companion object {
         val BRANDS: Int = 0
         val SIM: Int = 1
         val GPS: Int = 2
         val AUDIO_JACK: Int = 3
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FilterBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val behavior = BottomSheetBehavior.from(requireView().parent as View)
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initChipGroup(BRANDS)
        initChipGroup(SIM)
        initChipGroup(GPS)
        initChipGroup(AUDIO_JACK)
        initPriceSeekBar()

        binding.btApply.setOnClickListener {
            dismiss()
            mobileViewModel.filter()
        }
    }

    private fun initPriceSeekBar() {
        binding.priceSeekbar.setProgress(mobileViewModel.price.second.toFloat())
        binding.priceSeekbar.setOnSeekChangeListener(object : OnSeekChangeListener {
            override fun onSeeking(seekParams: SeekParams) {

            }
            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar) {}
            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {
                mobileViewModel.price = Pair(seekBar.min,seekBar.progress)
            }
        })
    }

    fun initChipGroup(type: Int) {
        var list: ArrayList<String> = ArrayList()
        mobileViewModel.filterHashMap.put(type,list)
        var textArray = getChipItemsList(type)
        var chipGroup: ChipGroup = getChipGroupLayout(type) as ChipGroup
        for (text: String? in textArray!!) {
            val chip = layoutInflater.inflate(R.layout.cat_chip_group_item_choice, chipGroup, false) as Chip
            chip.text = text
            chip.isCloseIconVisible = false
            chip.setOnClickListener {
                updateFilters(text, mobileViewModel.filterHashMap[type] as ArrayList<String>)
            }
            chipGroup.addView(chip)

        }
    }

    private fun getChipGroupLayout(type: Int): ViewGroup? {
        when (type) {
            BRANDS -> {
                return binding.cgBrand
            }
            SIM -> {
                return binding.cgSim
            }
            GPS -> {
                return binding.cgGps
            }
            AUDIO_JACK -> {
                return binding.cgAudiojack
            }
        }
        return null
    }

    fun getChipItemsList(type: Int): Array<String>? {
        when (type) {
            BRANDS -> {
                return resources.getStringArray(R.array.brand_chip_group_text_array)
            }
            SIM -> {
                return resources.getStringArray(R.array.sim_chip_group_text_array)
            }
            GPS -> {
                return resources.getStringArray(R.array.gps_chip_group_text_array)
            }
            AUDIO_JACK -> {
                return resources.getStringArray(R.array.audiojack_chip_group_text_array)
            }
        }
        return null
    }

    fun updateFilters(text: String?, filters: ArrayList<String>) {
        if (!isChipAdded(text, filters))
            text?.let { filters.add(it) }
        else
            filters.remove(text)
    }

    private fun isChipAdded(text: String?, list: ArrayList<String>): Boolean {
        for (i in list) {
            if (text.equals(i)) {
                return true
            }
        }
        return false
    }


    @LayoutRes
    protected fun getChipGroupItem(singleSelection: Boolean): Int {
        return if (singleSelection) R.layout.cat_chip_group_item_choice else R.layout.cat_chip_group_item_filter
    }

    interface OnActionCompleteListener {
        fun onActionComplete(str: String)
    }

}