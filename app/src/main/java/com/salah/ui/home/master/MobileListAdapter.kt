package com.salah.ui.home.master

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.salah.R
import com.salah.common.glide.load
import com.salah.common.utils.dp
import com.salah.databinding.RowMobileListBinding
import com.salah.model.MobileHandset
import com.salah.ui.FilterBottomSheet
import kotlinx.coroutines.runBlocking
import java.util.HashMap

class mobileListAdapter(val context: Context?, var items: List<MobileHandset> = ArrayList()) : RecyclerView.Adapter<ViewHolder>(),Filterable {
    var mobileFilteredList : List<MobileHandset> = ArrayList()
    lateinit var filtersList: HashMap<Int, List<String>>
    lateinit var priceFilter: Pair<Float, Int>
    interface OnItemClickListener {
        fun onItemClick(mobile: MobileHandset, container: View) // pass ImageView to make shared transition
    }

    private var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowMobileListBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mobile = mobileFilteredList[position]

        holder.tvMobileTitle.text = mobile.phone
        holder.tvPhonePrice.text =  "Price: €"+mobile.priceEur
        holder.ivMobilePoster.setImageResource(R.drawable.poster_placeholder)
        holder.ivMobilePoster.transitionName = mobile.id.toString()
        holder.llmobileTextContainer.setBackgroundColor(Color.DKGRAY)

        holder.ivMobilePoster.load(
            url = mobile.picture,
            crossFade = true, width = 160.dp, height = 160.dp) { color ->

            holder.llmobileTextContainer.setBackgroundColor(color)
        }

        holder.cvmobileContainer.setOnClickListener {
            onItemClickListener?.onItemClick(mobileFilteredList.get(position), holder.ivMobilePoster)
        }
    }

    override fun getItemCount(): Int {
        return mobileFilteredList.size
    }

    fun setOnmobileClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    fun fillList(items: List<MobileHandset>) {
        this.items = items
        mobileFilteredList = items
        notifyDataSetChanged()
    }

    fun clear() {
        this.items = emptyList()
    }

    fun toFloat(value: String): Float{
         if(value.isEmpty() || value == null) return 0f else return value.toFloat()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                mobileFilteredList = items
//                val charSearch = constraint.toString()
//                var resultList = ArrayList<MobileHandset>()
                 filterBrands(constraint.toString()) as ArrayList<MobileHandset>
//                if (charSearch.isEmpty()) {
//                    if(resultList.isEmpty()) mobileFilteredList = items as ArrayList<MobileHandset> else mobileFilteredList = resultList
//                } else {
//                    for (row in items) {
//                        if (row.brand?.toLowerCase()?.contains(constraint.toString().toLowerCase())!!) {
//                            resultList.add(row)
//                        }
//                    }
//                }
//                mobileFilteredList = resultList
                val filterResults = FilterResults()
                filterResults.values = mobileFilteredList
                return filterResults
            }

            private fun filterBrands(query: String): List<MobileHandset>{
                if(filtersList[FilterBottomSheet.BRANDS]?.isEmpty() == false)
                    runBlocking {  mobileFilteredList = items.filter {
                        filtersList[FilterBottomSheet.BRANDS]?.contains(it.brand)!!
                    }
                }
                if(filtersList[FilterBottomSheet.SIM]?.isEmpty() == false)
                    runBlocking {  mobileFilteredList = mobileFilteredList.filter {
                        filtersList[FilterBottomSheet.SIM]?.contains(it.sim) == true
                    }
                    }
                if(filtersList[FilterBottomSheet.GPS]?.isEmpty() == false)
                    runBlocking {  mobileFilteredList = mobileFilteredList.filter {
                        filtersList[FilterBottomSheet.GPS]?.contains(it.gps) == true
                    }
                    }
                if(filtersList[FilterBottomSheet.AUDIO_JACK]?.isEmpty() == false)
                    runBlocking {  mobileFilteredList = mobileFilteredList.filter {
                        filtersList[FilterBottomSheet.AUDIO_JACK]?.contains(it.gps) == true
                    }
                    }

                runBlocking {
                    mobileFilteredList = mobileFilteredList.filter {
                        toFloat(it.priceEur!!) >= priceFilter.first && toFloat(it.priceEur!!) <= priceFilter.second
                    }
                }
                runBlocking {
                    if(query.isEmpty() == false)
                    mobileFilteredList = mobileFilteredList.filter {
                        it.phone?.lowercase()?.contains(query.toString().lowercase())!!                    }
                }
                return mobileFilteredList
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results?.values  != null) {
                    mobileFilteredList = results.values as ArrayList<MobileHandset>
                }
                notifyDataSetChanged()
            }
        }
    }

    fun setFilters(filters: HashMap<Int, List<String>>,price : Pair<Float,Int>) {
        filtersList = filters
        priceFilter = price
    }
}

class ViewHolder (binding: RowMobileListBinding) : RecyclerView.ViewHolder(binding.root) {
    val cvmobileContainer: CardView = binding.cvMobileContainer
    val llmobileTextContainer: LinearLayout = binding.llTextContainer
    val tvMobileTitle: TextView = binding.tvMobileTitle
    val tvPhonePrice: TextView = binding.tvPhonePrice
    val ivMobilePoster: ImageView = binding.ivMobilePoster
}