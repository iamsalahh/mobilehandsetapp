package com.salah.ui.home.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.salah.data.Resource
import com.salah.domain.interactor.GetMobileUseCase
import com.salah.model.MobileHandset
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MobileViewModel(private val getPopularmobilesUseCase: GetMobileUseCase) : ViewModel() {

    lateinit var mobileHandset: MobileHandset
    private val stateFlow = MutableStateFlow<Resource<List<MobileHandset>>>(Resource.empty())
    val searchLiveData = MutableLiveData<HashMap<Int,List<String>>>(HashMap())
    var mobileHandsetLiveData:  MutableLiveData<List<MobileHandset>> = MutableLiveData(ArrayList())
    var price : Pair<Float, Int> = Pair(100f,5000)
    var filterHashMap : HashMap<Int, List<String>>
            = HashMap<Int, List<String>> ()
    var disposable: Disposable? = null

    val popularmobilesState: StateFlow<Resource<List<MobileHandset>>>
        get() = stateFlow
    fun fetchPopularmobiles() {
        stateFlow.value = Resource.loading()

        disposable = getPopularmobilesUseCase.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ res ->
                stateFlow.value = Resource.success(res)
                mobileHandsetLiveData.value = res
            }, { throwable ->
                throwable.localizedMessage?.let {
                    stateFlow.value = Resource.error(it)
                }
            })
    }

    fun refreshPopularmobiles() {
        fetchPopularmobiles()
    }

    fun filter() {
        searchLiveData.postValue(filterHashMap)
    }
}