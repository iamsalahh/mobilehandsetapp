package com.salah.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.ActivityNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.salah.R
import com.salah.common.utils.WindowPreferencesManager
import com.salah.common.utils.gone
import com.salah.data.Resource
import com.salah.databinding.FragmentMobileListBinding
import com.salah.model.MobileHandset
import com.salah.ui.home.master.mobileListAdapter
import com.salah.ui.home.viewmodel.MobileViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import timber.log.Timber
import java.util.*

class MobileListFragment : Fragment(R.layout.fragment_mobile_list),
    mobileListAdapter.OnItemClickListener {

    private val mobileViewModel: MobileViewModel by sharedViewModel()
    private val mobileListAdapter: mobileListAdapter by inject()

    private var windowPreferencesManager: WindowPreferencesManager? = null
    private var _binding: FragmentMobileListBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        windowPreferencesManager = WindowPreferencesManager(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMobileListBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupRecyclerView()
        setupSwipeRefresh()
        setupSearch()
        binding.searchBoxContainer.filterSearchQuery.setOnClickListener { setupBottomsheet() }
        mobileViewModel.refreshPopularmobiles()

        viewLifecycleOwner.lifecycleScope.launch {
            mobileViewModel.popularmobilesState.collect {
                handlemobilesDataState(it)

            }
        }
    }

    private fun setupBottomsheet() {
        val action = MobileListFragmentDirections.navigateToBottomsheet()
        findNavController().navigate(action, ActivityNavigatorExtras())
    }

    private fun setupSearch() {
        binding.searchBoxContainer.searchEditText.doOnTextChanged { text, _, _, _ ->
            val query = text.toString().lowercase()
            mobileListAdapter.filter.filter(query)
        }
        mobileViewModel.searchLiveData.observeForever {
            filterRecyclerview(it, mobileViewModel.price)
        }
    }
    private fun filterRecyclerview(hashMap: HashMap<Int, List<String>>, price: Pair<Float, Int>) {
        mobileListAdapter.setFilters(hashMap, price)
        mobileListAdapter.filter.filter(_binding?.searchBoxContainer?.searchEditText?.text)
    }

    override fun onItemClick(mobileHandset: MobileHandset, container: View) {
        Toast.makeText(context,"Brand: " + mobileHandset.brand + " Model: " + mobileHandset.phone
                + " Resolution: " + mobileHandset.resolution ,Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

        mobileViewModel.disposable?.dispose()
    }

    private fun handlemobilesDataState(state: Resource<List<MobileHandset>>) {
        when (state.status) {
            Resource.Status.LOADING -> {
                binding.srlFragmentMobileList.isRefreshing = true
            }
            Resource.Status.SUCCESS -> {
                binding.srlFragmentMobileList.isRefreshing = false
                loadmobiles(state.data)
            }
            Resource.Status.ERROR -> {
                binding.srlFragmentMobileList.isRefreshing = false
                binding.pbFragmentMobileList.gone()
                Snackbar.make(
                    binding.srlFragmentMobileList,
                    getString(R.string.error_message_pattern, state.message),
                    Snackbar.LENGTH_LONG
                )
                    .show()
            }
            Resource.Status.EMPTY -> {
                Timber.d("Empty state.")
            }
        }
    }
    private fun loadmobiles(mobiles: List<MobileHandset>?) {
        mobiles?.let {
            mobileListAdapter.fillList(it)
        }
    }

    private fun setupRecyclerView() {
        mobileListAdapter.setOnmobileClickListener(this)

        binding.rvFragmentMobileList.adapter = mobileListAdapter

    }

    private fun setupSwipeRefresh() {
        binding.srlFragmentMobileList.setOnRefreshListener {
            mobileViewModel.refreshPopularmobiles()
        }
    }
}