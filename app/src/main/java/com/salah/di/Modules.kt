package com.salah.di

import com.salah.data.repository.MobileRemoteRepositoryImpl
import com.salah.domain.interactor.*
import com.salah.domain.repository.MobileRemoteRepository
import com.salah.ui.home.master.mobileListAdapter
import com.salah.ui.home.viewmodel.MobileViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    factory<MobileRemoteRepository> { MobileRemoteRepositoryImpl() }
    factory { mobileListAdapter(androidContext()) }
}

val popularmobilesModule = module {
    factory { GetMobileUseCase(get()) }
    viewModel { MobileViewModel(get()) }
}


val mobileDetailsModule = module {
}