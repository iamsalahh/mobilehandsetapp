package com.salah.data.sources.remote.service

import com.salah.model.MobileHandset
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MobileHandsetService {

    @GET("https://jsonkeeper.com/b/UKQI")
    fun getMobileHandsets(): Single<List<MobileHandset>>
}