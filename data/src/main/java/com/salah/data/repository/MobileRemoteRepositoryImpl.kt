package com.salah.data.repository

import com.salah.data.sources.remote.api.ApiClient
import com.salah.domain.repository.MobileRemoteRepository
import com.salah.model.MobileHandset
import io.reactivex.Single

class MobileRemoteRepositoryImpl() : MobileRemoteRepository  {

    override fun getMobileHandsets(): Single<List<MobileHandset>> {
        return ApiClient.mobileservice().getMobileHandsets()
    }
}