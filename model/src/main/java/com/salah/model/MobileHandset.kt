package com.salah.model

import com.google.gson.annotations.SerializedName
data class MobileHandset (

  @SerializedName("id"           ) var id           : String?    = null,
  @SerializedName("brand"        ) var brand        : String? = null,
  @SerializedName("phone"        ) var phone        : String? = null,
  @SerializedName("picture"      ) var picture      : String? = null,
  @SerializedName("announceDate" ) var announceDate : String? = null,
  @SerializedName("sim"          ) var sim          : String? = null,
  @SerializedName("resolution"   ) var resolution   : String? = null,
  @SerializedName("audioJack"    ) var audioJack    : String? = null,
  @SerializedName("gps"          ) var gps          : String? = null,
  @SerializedName("battery"      ) var battery      : String? = null,
  @SerializedName("priceEur"     ) var priceEur     : String?    = null

)